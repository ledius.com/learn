import { Component } from '@angular/core';
import {CourseListItem} from "../../model/course-list-item";
import {ActivatedRoute} from "@angular/router";
import {Lesson} from "../../model/lesson/lesson";
import {CoursePurchase} from "../../model/course-purchase/course-purchase";
import {CoursePurchasesService} from "../../services/course-purchases.service";

@Component({
  selector: 'app-course-view',
  templateUrl: './course-view.component.html',
  styleUrls: ['./course-view.component.scss']
})
export class CourseViewComponent {
  public course: CourseListItem;
  public lessons: Lesson[] = [];
  public coursePurchases: CoursePurchase[] = [];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly coursePurchasesService: CoursePurchasesService,
  ) {
    this.course = this.route.snapshot.data.course;
    this.lessons = this.route.snapshot.data.lessons || [];
    this.coursePurchasesService.$userCoursePurchased.subscribe(purchases => this.coursePurchases = purchases);
  }

  public getCoursePurchase(course: CourseListItem): CoursePurchase | null {
    return this.coursePurchases.find(purchase => purchase.purchasedForCourse(course.id)) ?? null;
  }
}
