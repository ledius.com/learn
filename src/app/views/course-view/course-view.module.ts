import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseViewComponent } from './course-view.component';
import {RouterModule} from "@angular/router";
import {CourseExplainCardModule} from "../../course-explain-card/course-explain-card.module";



@NgModule({
  declarations: [
    CourseViewComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    CourseExplainCardModule
  ]
})
export class CourseViewModule { }
