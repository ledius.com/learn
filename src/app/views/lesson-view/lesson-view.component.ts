import { Component } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Lesson} from "../../model/lesson/lesson";

@Component({
  selector: 'app-lesson-view',
  templateUrl: './lesson-view.component.html',
  styleUrls: ['./lesson-view.component.scss']
})
export class LessonViewComponent {

  public lesson: Lesson | null = null;

  constructor(private readonly route: ActivatedRoute) {
    this.lesson = this.route.snapshot.data.lesson;
  }
}
