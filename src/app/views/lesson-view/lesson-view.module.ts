import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LessonViewComponent } from './lesson-view.component';
import {LessonContentModule} from "../../lesson-content/lesson-content.module";



@NgModule({
  declarations: [
    LessonViewComponent
  ],
  imports: [
    CommonModule,
    LessonContentModule
  ]
})
export class LessonViewModule { }
