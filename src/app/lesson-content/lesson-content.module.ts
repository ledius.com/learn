import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LessonPageComponent} from "../lesson-page/lesson-page.component";



@NgModule({
  declarations: [LessonPageComponent],
  imports: [
    CommonModule
  ],
  exports: [LessonPageComponent]
})
export class LessonContentModule { }
