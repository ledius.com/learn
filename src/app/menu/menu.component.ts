import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Balance} from "../model/account/balance";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {

  @Input()
  public opened: boolean = false;

  @Output()
  public openedChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input()
  public isAuth = true

  @Input()
  public balance: Balance = Balance.empty('LDS');

  @Input()
  public profileName = ''

  @Input()
  public profileEmail = ''

  @Input()
  public profileAvatar = '';

  @Output()
  public login: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  public logout: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  public menuClickHandler(): void {
    this.openedChange.emit(!this.opened);
  }

  public async googleAuth(): Promise<void> {
    await this.login.emit();
  }

  public async exitHandler(): Promise<void> {
    this.logout.emit();
    this.openedChange.emit(false);
  }
}
