import { Component } from '@angular/core';
import {CourseListItem} from "../model/course-list-item";
import {TagListItem} from "../model/tag/tag-list-item";
import {CoursesService} from "../services/courses.service";
import {CoursePurchase} from "../model/course-purchase/course-purchase";
import {CoursePurchasesService} from "../services/course-purchases.service";
import {TagService} from "../services/tag.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent  {
  isListOpen = false
  selectedFilter = 'all'
  selectedFilterLabel = 'Все'

  public tags: TagListItem[] = [];

  public courses: CourseListItem[];

  public coursePurchases: CoursePurchase[];

  constructor(
    private readonly coursesService: CoursesService,
    private readonly coursePurchasesService: CoursePurchasesService,
    private readonly tagService: TagService,
    private readonly location: Location,
  ) {
    this.coursesService.$courses.subscribe(courses => this.courses = courses);
    this.coursePurchasesService.$userCoursePurchased.subscribe(purchases => this.coursePurchases = purchases);
    this.tagService.$tags.subscribe(tags => this.tags = tags);
  }

  showAllClickHandler() {
    this.isListOpen = !this.isListOpen
    console.log(this.isListOpen)
  }

  setSelectedFilterLabel(e, type) {
    this.selectedFilterLabel = e.target.innerHTML
    this.selectedFilter = type
  }

  public async onActivateTag(tag: TagListItem): Promise<void> {
    await this.coursesService.fetch({ tag: tag.id });
  }

  public async onResetTag(): Promise<void> {
    await this.coursesService.fetch();
  }
}
