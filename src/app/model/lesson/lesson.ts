export class Lesson {
  public readonly id: string;
  public readonly content: { name: string, icon: string, text: string };
  public readonly courseId: string;
  public readonly isDraft: boolean;
  public readonly isPublished: boolean;
}
