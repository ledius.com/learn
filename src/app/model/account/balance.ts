import { Transform } from "class-transformer";

export class Balance {
  @Transform(({value}) => {
    return BigInt(value);
  })
  public readonly balance: bigint;

  public readonly symbol: string;

  constructor(balance: bigint, symbol: string) {
    this.balance = balance;
    this.symbol = symbol;
  }

  public isLDS() {
    return this.symbol === 'LDS';
  }

  public toFixed(): number {
    return Number(
      this.balance / BigInt(10 ** 18),
    );
  }

  public static empty(symbol: string): Balance {
    return new Balance(BigInt(0), symbol);
  }

  public formattedBalance(): string {

    const balance = String(this.balance);
    const decimals = 18;

    const [scale, precision] = [
      BigInt(balance) / BigInt(10 ** decimals),
      balance
        .split('')
        .slice(-18)
        .join('')
        .padStart(decimals, '0')
    ];
    return `${scale}.${precision}`;

  }
}
