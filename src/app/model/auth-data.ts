export class AuthData {
  public readonly id: string;
  public readonly email: string;
}
