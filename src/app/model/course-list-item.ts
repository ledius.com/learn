import {TagListItem} from "./tag/tag-list-item";
import {Type} from "class-transformer";

export class CourseListItem {
  public readonly id: string;
  public readonly content: { name: string, icon: string, description: string };
  public readonly bought: boolean = false;
  @Type(() => TagListItem)
  public readonly tag?: TagListItem;
  public readonly prices: {
    lds: string;
  };
  public readonly progress: number = 0;
  public readonly chips: { text:  string, icon: string }[];

  constructor(data: Partial<CourseListItem> = {}) {
    this.id = data.id;
    this.content = data.content;
    this.bought = data.bought;
    this.prices = data.prices;
    this.progress = data.progress;
  }

  public get shortLdsPrice(): number {
    return Number(BigInt(this.prices.lds) / BigInt(10 ** 18))
  }
}
