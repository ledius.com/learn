export class CoursePurchase {
  public readonly id: string;
  public readonly userId: string;
  public readonly courseId: string;
  public readonly isPaid: boolean;

  public purchasedForCourse(courseId: string): boolean {
    return this.isPaid && this.courseId === courseId;
  }
}
