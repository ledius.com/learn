import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

import { PageNotFoundComponent } from './error-routing/not-found/not-found.component';
import { UncaughtErrorComponent } from './error-routing/error/uncaught-error.component';
import { ErrorRoutingModule } from './error-routing/error-routing.module';
import {CourseViewComponent} from "./views/course-view/course-view.component";
import {HomeViewResolver} from "./resolvers/home-view.resolver";
import {CourseViewResolver} from "./resolvers/course-view.resolver";
import {LessonsListResolver} from "./resolvers/lessons-list.resolver";
import {LessonViewComponent} from "./views/lesson-view/lesson-view.component";
import {LessonResolver} from "./resolvers/lesson.resolver";
import {CoursePurchasesResolver} from "./resolvers/course-purchases.resolver";
import {TagResolver} from "./resolvers/tag.resolver";

export const routes: Routes = [
  { path: '',component: HomeComponent, pathMatch: 'full', resolve: [HomeViewResolver, TagResolver]},
  {
    path: 'course/:courseId',
    component: CourseViewComponent,
    resolve: {
      course: CourseViewResolver,
      lessons: LessonsListResolver,
      coursePurchases: CoursePurchasesResolver
    }
  },
  { path: 'lesson/:lessonId', component: LessonViewComponent, resolve: { lesson: LessonResolver } },
  { path: 'error', component: UncaughtErrorComponent },
  { path: '**', component: PageNotFoundComponent } // must always be last
];

@NgModule({
  imports: [RouterModule.forRoot(routes), ErrorRoutingModule],
  exports: [RouterModule, ErrorRoutingModule]
})
export class AppRoutingModule { }
