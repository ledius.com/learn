import {Pipe} from '@angular/core';

@Pipe({name: 'toFixed'})
export class ToFixedPipe {
  transform (input:number) {
    return input.toFixed(1);
  }
}