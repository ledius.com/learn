import { Component, Input } from '@angular/core';
import { Lesson } from "../model/lesson/lesson";

@Component({
  selector: 'app-lesson-page',
  templateUrl: './lesson-page.component.html',
  styleUrls: ['./lesson-page.component.scss']
})
export class LessonPageComponent {
  @Input() public lesson: Lesson;

  constructor() { }

}
