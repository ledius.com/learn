import { Component } from '@angular/core';
import {Toast, ToastService} from "../services/toast.service";

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss']
})
export class ToastComponent {

  public toasts: Toast[];

  constructor(
    private readonly toastService: ToastService,
  ) {
    this.toastService.$toasts.subscribe(toasts => this.toasts = toasts);
  }

  public async close(toast: Toast): Promise<void> {
    await this.toastService.remove(toast);
  }
}
