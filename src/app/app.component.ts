import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {AuthService} from "./services/auth.service";
import { AccountService } from "./services/account.service";
import {Balance} from "./model/account/balance";
import {Location} from "@angular/common";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  public isMenuOpen = false
  public isAuth = true
  public balance: Balance = Balance.empty('LDS');
  public profileName = ''
  public profileEmail = ''
  public profileAvatar = '';


  constructor(
    private readonly authService: AuthService,
    private readonly accountService: AccountService,
    private readonly location: Location,
  ) {
    this.accountService.$userBalances.subscribe(balances => {
      const ldsBalance = balances.find(balance => balance.isLDS());

      if(ldsBalance) {
        this.balance = ldsBalance;
      }
    })
  }

  public goBack(): void {
    this.location.back();
  }

  public ngOnInit(): void {
    this.authService.$socialUser.subscribe(socialUser => {
      if(!socialUser) {
        this.isAuth = false;
        return;
      }
      this.isAuth = true;
      this.profileEmail = socialUser.email;
      this.profileName = socialUser.name;
      this.profileAvatar = socialUser.photoUrl;
    });
  }

  menuClickHandler() {
    this.isMenuOpen = !this.isMenuOpen
  }

  public async onLogin(): Promise<void> {
    await this.authService.logIn();
  }

  public async onLogout(): Promise<void> {
    await this.authService.logOut();
    this.isMenuOpen = false
  }
}
