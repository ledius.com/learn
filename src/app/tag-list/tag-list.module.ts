import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagListComponent } from './tag-list.component';
import { TagListItemComponent } from './tag-list-item/tag-list-item.component';



@NgModule({
  declarations: [
    TagListComponent,
    TagListItemComponent
  ],
  exports: [
    TagListComponent
  ],
  imports: [
    CommonModule
  ]
})
export class TagListModule { }
