import {Component, EventEmitter, Input, Output} from '@angular/core';
import {TagListItem} from "../model/tag/tag-list-item";

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss']
})
export class TagListComponent {

  @Input() public tags: TagListItem[] = [];
  @Output() public activate: EventEmitter<TagListItem> = new EventEmitter<TagListItem>();
  @Output() public resetTag: EventEmitter<void> = new EventEmitter<void>();

  public isListOpen: boolean = false;
  public activeId: string | null;

  public toggleOpen(): void {
    this.isListOpen = !this.isListOpen;
  }

  public isActive(tagId: string): boolean {
    return this.activeId === tagId;
  }

  public makeActive(e: TagListItem | null): void {
    this.activeId = e ? e.id : null;
    if(this.activeId) {
      this.activate.emit(e);
    } else {
      this.resetTag.emit();
    }
  }
}
