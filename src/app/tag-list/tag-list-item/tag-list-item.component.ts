import {Component, EventEmitter, Input, Output} from '@angular/core';
import {TagListItem} from "../../model/tag/tag-list-item";


@Component({
  selector: 'app-tag-list-item',
  templateUrl: './tag-list-item.component.html',
  styleUrls: ['./tag-list-item.component.scss']
})
export class TagListItemComponent  {

  @Input() public tag: TagListItem;
  @Input() public active: boolean = false;
  @Output() public clickTag: EventEmitter<TagListItem> = new EventEmitter<TagListItem>();

  constructor() { }

   public onClick(): void {
    if(this.active) {
      this.clickTag.emit(null);
    } else {
      this.clickTag.emit(this.tag);
    }
  }

}
