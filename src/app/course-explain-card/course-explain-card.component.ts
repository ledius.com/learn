import {Component, Input} from '@angular/core';
import {CourseListItem} from "../model/course-list-item";
import {Lesson} from "../model/lesson/lesson";
import {Router} from "@angular/router";
import {CoursePurchase} from "../model/course-purchase/course-purchase";
import {CoursePurchasesService} from "../services/course-purchases.service";

@Component({
  selector: 'app-course-explain-card',
  templateUrl: './course-explain-card.component.html',
  styleUrls: ['./course-explain-card.component.scss']
})
export class CourseExplainCardComponent {

  @Input() public course: CourseListItem;

  @Input() public lessons: Lesson[];

  @Input() public coursePurchase: CoursePurchase | null;

  public paymentProcessing: boolean = false;

  constructor(
    private readonly router: Router,
    private readonly coursePurchasesService: CoursePurchasesService
  ) {}

  public get bought(): boolean {
    return this.coursePurchase && this.course.id === this.coursePurchase.courseId;
  }

  public async buyCourse(): Promise<void> {
    if(this.bought || this.paymentProcessing) {
      return;
    }
    try {
      this.paymentProcessing = true;
      await this.coursePurchasesService.buyLds(this.course.id);
    } finally {
      this.paymentProcessing = false;
    }
  }

  public async openLesson(lesson: Lesson): Promise<void> {
    if(this.bought) {
      await this.router.navigateByUrl(`/lesson/${lesson.id}`)
    }
  }
}
