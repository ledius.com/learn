import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseExplainCardComponent } from './course-explain-card.component';
import {RouterModule} from "@angular/router";



@NgModule({
    declarations: [
        CourseExplainCardComponent
    ],
    exports: [
        CourseExplainCardComponent
    ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class CourseExplainCardModule { }
