import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseExplainCardComponent } from './course-explain-card.component';

describe('CourseExplainCardComponent', () => {
  let component: CourseExplainCardComponent;
  let fixture: ComponentFixture<CourseExplainCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourseExplainCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseExplainCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
