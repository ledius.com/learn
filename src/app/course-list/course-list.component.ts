import {Component, Input} from '@angular/core';
import {CourseListItem} from "../model/course-list-item";
import {animate, style, transition, trigger} from "@angular/animations";
import {CoursePurchasesService} from "../services/course-purchases.service";
import {CoursePurchase} from "../model/course-purchase/course-purchase";

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss'],
  animations: [
    trigger('inOut', [
      transition(':enter', [
        style({  opacity: 0.5 }),
        animate('0.3s', style({ opacity: 1 }))
      ])
    ])
  ]
})
export class CourseListComponent {
  @Input()
  public courses: CourseListItem[];

  @Input()
  public coursePurchases: CoursePurchase[] = [];

  constructor() {}

  public getCoursePurchaseForCourse(course: CourseListItem): CoursePurchase | null {
    return this.coursePurchases.find(purchase => purchase.purchasedForCourse(course.id)) ?? null;
  }
}
