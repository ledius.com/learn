import {Component, Input, OnInit} from '@angular/core';
import {CourseListItem} from "../../model/course-list-item";
import {CoursePurchase} from "../../model/course-purchase/course-purchase";

@Component({
  selector: 'app-course-list-item',
  templateUrl: './course-list-item.component.html',
  styleUrls: ['./course-list-item.component.scss']
})
export class CourseListItemComponent {

  @Input()
  public course: CourseListItem;

  @Input()
  public coursePurchase: CoursePurchase | null = null;


  public get bought(): boolean {
    return this.coursePurchase && this.coursePurchase.isPaid && this.coursePurchase.courseId === this.course.id;
  }

  public get intPercent(): number {
    return this.course.progress * 100;
  }

  public get progressBackground() {
    return {
      background: this.course.bought
        ? 'linear-gradient(to right, #4484FE ' + (this.intPercent) + '%, transparent ' + (this.intPercent) + '%)'
        : ''
    }
  }
}
