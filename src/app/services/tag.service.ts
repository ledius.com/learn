import { Injectable } from '@angular/core';
import {BehaviorSubject, lastValueFrom, map, tap} from "rxjs";
import {TagListItem} from "../model/tag/tag-list-item";
import {HttpClient} from "@angular/common/http";
import {plainToInstance} from "class-transformer";

@Injectable({
  providedIn: 'root'
})
export class TagService {

  public readonly $tags: BehaviorSubject<TagListItem[]> = new BehaviorSubject<TagListItem[]>([]);

  constructor(
    private readonly http: HttpClient
  ) { }

  public async fetch(): Promise<TagListItem[]> {
    return await lastValueFrom(
      this.http.get<TagListItem[]>('/api/v1/tags')
        .pipe(
          map(tags => plainToInstance(TagListItem, tags)),
          tap(tags => this.$tags.next(tags))
        )
    )
  }
}
