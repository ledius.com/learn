import { Injectable } from '@angular/core';
import {Lesson} from "../model/lesson/lesson";
import {BehaviorSubject, lastValueFrom, map, tap} from "rxjs";
import {HttpClient, HttpParams} from "@angular/common/http";
import {plainToInstance} from "class-transformer";

@Injectable({
  providedIn: 'root'
})
export class LessonsService {

  public readonly $lessons: BehaviorSubject<Lesson[]> = new BehaviorSubject<Lesson[]>([]);

  constructor(
    private readonly http: HttpClient
  ) {}

  public async fetchLessons(courseId?: string): Promise<Lesson[]> {
    let params = new HttpParams();
    if(courseId) {
      params = params.set('courseId', courseId);
    }
    return await lastValueFrom(
      this.http.get<{items: unknown[]}>(
        `/api/v1/lessons`, {
          params
        }
      ).pipe(
        map(({items}) => plainToInstance(Lesson, items)),
        tap(lessons => this.$lessons.next(lessons))
      )
    )
  }

  public async getById(lessonId: string): Promise<Lesson> {
    return await lastValueFrom(
      this.http.get<Lesson>(`/api/v1/lesson/${lessonId}`)
        .pipe(
          map(lesson => plainToInstance(Lesson, lesson)),
        )
    )
  }
}
