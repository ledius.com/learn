import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";

export interface Toast {
  readonly text: string;
  readonly duration?: number;
}

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  public readonly $toasts: BehaviorSubject<Toast[]> = new BehaviorSubject<Toast[]>([]);

  constructor() { }

  public async remove(...toasts: Toast[]): Promise<void> {
    this.$toasts.next(
      this.$toasts.getValue().filter(toast => !toasts.includes(toast))
    )
  }

  public async push(...toasts: Toast[]): Promise<void> {
    this.$toasts.next([
      ...this.$toasts.getValue(),
      ...toasts,
    ]);
    this.scheduleRemove(...toasts);
  }

  private scheduleRemove(...toasts: Toast[]): void {
    setTimeout(() => this.remove(...toasts), 5000);
  }
}
