import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {BehaviorSubject, filter, lastValueFrom, map, mergeMap, Observable, tap} from "rxjs";
import {CourseListItem} from "../model/course-list-item";
import {plainToInstance} from "class-transformer";

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  public readonly $courses: BehaviorSubject<CourseListItem[]> = new BehaviorSubject<CourseListItem[]>([]);

  constructor(
    protected readonly http: HttpClient
  ) { }

  public async fetch({tag}: {tag?: string} = {}) : Promise<void> {
    let params = new HttpParams();
    if(tag) {
      params = params.set('tag', tag);
    }
    await lastValueFrom(this.http.get<{items: unknown[]}>('/api/v1/courses', { params })
        .pipe(
          map(({items}) => plainToInstance(CourseListItem, items)),
          tap(courses => this.$courses.next(courses))
        ))
  }

  public pushCourse(course: CourseListItem): void {
    this.$courses.next([...this.$courses.getValue(), course]);
  }

  public async fetchById(id: string): Promise<CourseListItem> {
    const course = this.$courses.getValue().find(course => course.id === id);
    if(course) {
      return course;
    }

    return await lastValueFrom(this.http.get<CourseListItem>(`/api/v1/courses/${id}`)
      .pipe(
        map(course => plainToInstance(CourseListItem, course)),
        tap(course => this.pushCourse(course))
      )
    )
  }
}
