import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {CoursePurchase} from "../model/course-purchase/course-purchase";
import {BehaviorSubject, lastValueFrom, map, tap} from "rxjs";
import {plainToInstance} from "class-transformer";
import {AuthService} from "./auth.service";
import {AccountService} from "./account.service";

@Injectable({
  providedIn: 'root'
})
export class CoursePurchasesService {

  public readonly $userCoursePurchased: BehaviorSubject<CoursePurchase[]> = new BehaviorSubject<CoursePurchase[]>([]);

  constructor(
    private readonly http: HttpClient,
    private readonly authService: AuthService,
    private readonly accountService: AccountService,
  ) {
    this.authService.$authInfo.subscribe(async authInfo => {
      if(!authInfo) return;

      await this.fetchUserCoursePurchases(authInfo.id);
    })
  }

  public async fetchUserCoursePurchases(userId: string): Promise<CoursePurchase[]> {
    return await lastValueFrom(
      this.http.get<{ items: unknown[] }>(
        `/api/v1/purchases/course`, {
          params: new HttpParams().set('userId', userId)
        })
        .pipe(
          map(({items}) => plainToInstance(CoursePurchase, items)),
          tap(purchases => this.$userCoursePurchased.next(purchases))
        )
    )
  }

  public async buyLds(courseId: string): Promise<CoursePurchase> {
    return await lastValueFrom(
      this.http.post<CoursePurchase>(`/api/v1/purchase/course/buy/lds`, { courseId })
        .pipe(
          map((purchase) => plainToInstance(CoursePurchase, purchase)),
          tap(() => this.fetchUserCoursePurchases(this.authService.$authInfo.getValue()?.id)),
          tap(() => this.accountService.refreshBalances()),
        )
    )
  }
}
