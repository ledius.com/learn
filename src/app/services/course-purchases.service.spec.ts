import { TestBed } from '@angular/core/testing';

import { CoursePurchasesService } from './course-purchases.service';

describe('CoursePurchasesService', () => {
  let service: CoursePurchasesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoursePurchasesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
