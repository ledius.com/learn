import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpResponse
} from '@angular/common/http';
import {from, mergeMap, Observable, of} from 'rxjs';
import {ToastService} from "../services/toast.service";

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(
    private readonly toastService: ToastService,
  ) {}

  public intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      mergeMap(response => {
      if(response instanceof HttpResponse) {
        if(response.status >= 400) {
          const message = response.body['message'] || response.statusText;
          from(this.toastService.push({ text: message }));
        }
      }

      return of(response);
    }));
  }
}
