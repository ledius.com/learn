import { TestBed } from '@angular/core/testing';

import { LessonsListResolver } from './lessons-list.resolver';

describe('LessonsListResolver', () => {
  let resolver: LessonsListResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(LessonsListResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
