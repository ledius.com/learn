import { TestBed } from '@angular/core/testing';

import { HomeViewResolver } from './course-view.resolver';

describe('CourseViewResolver', () => {
  let resolver: HomeViewResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(HomeViewResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
