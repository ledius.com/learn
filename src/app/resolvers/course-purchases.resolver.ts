import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {CoursePurchase} from "../model/course-purchase/course-purchase";
import {CoursePurchasesService} from "../services/course-purchases.service";
import {AuthService} from "../services/auth.service";

@Injectable({
  providedIn: 'root'
})
export class CoursePurchasesResolver implements Resolve<CoursePurchase[]> {
  constructor(
    private readonly coursePurchasesService: CoursePurchasesService,
    private readonly authService: AuthService,
  ) {
  }

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<CoursePurchase[]> {
    const userId = this.authService.$authInfo.getValue()?.id;
    if(!userId) {
      return [];
    }
    return this.coursePurchasesService.fetchUserCoursePurchases(userId);
  }
}
