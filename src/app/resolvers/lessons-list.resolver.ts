import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {Lesson} from "../model/lesson/lesson";
import {LessonsService} from "../services/lessons.service";

@Injectable({
  providedIn: 'root'
})
export class LessonsListResolver implements Resolve<Lesson[]> {
  constructor(
    private readonly lessonsService: LessonsService
  ) {}

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Lesson[]> {
    return this.lessonsService.fetchLessons(route.params.courseId);
  }
}
