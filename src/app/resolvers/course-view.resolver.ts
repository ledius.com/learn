import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {CourseListItem} from "../model/course-list-item";
import {CoursesService} from "../services/courses.service";

@Injectable({
  providedIn: 'root'
})
export class CourseViewResolver implements Resolve<CourseListItem | null> {
  constructor(
    private readonly coursesService: CoursesService
  ) {
  }

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<CourseListItem | null> {
    return this.coursesService.fetchById(route.params.courseId);
  }
}
