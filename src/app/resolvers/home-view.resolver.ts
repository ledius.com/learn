import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {CoursesService} from "../services/courses.service";

@Injectable({
  providedIn: 'root'
})
export class HomeViewResolver implements Resolve<void> {
  constructor(
    private readonly coursesService: CoursesService
  ) {}

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<void> {
    await this.coursesService.fetch();
  }
}
