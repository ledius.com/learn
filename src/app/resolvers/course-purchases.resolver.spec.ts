import { TestBed } from '@angular/core/testing';

import { CoursePurchasesResolver } from './course-purchases.resolver';

describe('CoursePurchasesResolver', () => {
  let resolver: CoursePurchasesResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(CoursePurchasesResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
