import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {TagListItem} from "../model/tag/tag-list-item";
import {TagService} from "../services/tag.service";

@Injectable({
  providedIn: 'root'
})
export class TagResolver implements Resolve<TagListItem[]> {

  constructor(
    private readonly tagService: TagService,
  ) {}

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<TagListItem[]> {
    return this.tagService.fetch();
  }
}
