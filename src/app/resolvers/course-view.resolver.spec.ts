import { TestBed } from '@angular/core/testing';

import { CourseViewResolver } from './course-view.resolver';

describe('CourseViewResolver', () => {
  let resolver: CourseViewResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(CourseViewResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
