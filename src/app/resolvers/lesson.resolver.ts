import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {Lesson} from "../model/lesson/lesson";
import {LessonsService} from "../services/lessons.service";

@Injectable({
  providedIn: 'root'
})
export class LessonResolver implements Resolve<Lesson> {
  constructor(
    private readonly lessonsService:  LessonsService
  ) {
  }

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Lesson> {
    return this.lessonsService.getById(route.params.lessonId);
  }
}
