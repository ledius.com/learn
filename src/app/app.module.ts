import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IgxLayoutModule, IgxNavbarModule, IgxNavigationDrawerModule, IgxRippleModule } from 'igniteui-angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CourseComponent } from './course/course.component';
import { HomeComponent } from './home/home.component';
import { ToFixedPipe } from './pipes/toFixed.pipe';
import {CourseListModule} from "./course-list/course-list.module";
import {TagListModule} from "./tag-list/tag-list.module";
import {CourseViewModule} from "./views/course-view/course-view.module";
import {GoogleLoginProvider, SocialAuthServiceConfig, SocialLoginModule} from "angularx-social-login";
import {environment} from "../environments/environment";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AuthInterceptor} from "./interceptors/auth.interceptor";
import {LessonViewModule} from "./views/lesson-view/lesson-view.module";
import {MenuModule} from "./menu/menu.module";
import {ToastModule} from "./toast/toast.module";
import {HttpErrorInterceptor} from "./interceptors/http-error.interceptor";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CourseComponent,
    ToFixedPipe,
  ],
    imports: [
        FormsModule,
        BrowserModule,
        BrowserAnimationsModule,
        HammerModule,
        AppRoutingModule,
        CourseViewModule,
        IgxLayoutModule,
        IgxNavbarModule,
        IgxNavigationDrawerModule,
        IgxRippleModule,
        CourseListModule,
        TagListModule,
        SocialLoginModule,
        HttpClientModule,
        LessonViewModule,
        MenuModule,
        ToastModule,
    ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: true,
        providers: [
          {
            provider: new GoogleLoginProvider(environment.googleAuthClientId),
            id: GoogleLoginProvider.PROVIDER_ID
          }
        ]
      } as SocialAuthServiceConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
